#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Classe pour dessiner le terrain
"""

from PyQt4 import QtGui
from PyQt4.QtCore import Qt

class GridDisplay(QtGui.QWidget):
    """
    Affichage des fourmis, de la nourriture, etc.
    """

    def __init__(self, dim):
        """ initialisation """
        super(GridDisplay, self).__init__()
        self.dim = dim
        self.coordXmax = self.dim[0]
        self.coordYmax = self.dim[1]

        #central widget pour la fenêtre principale
        cwidget = QtGui.QWidget(self)
        
        # On met un layout sur lequel ajouter le canvas (et des futurs boutons(?))
        layout = QtGui.QVBoxLayout()
        #le canvas qu'on ajoute au layout
        canvas = QtGui.QGraphicsView()
        layout.addWidget(canvas)
        #et on définit le layout du widget
        cwidget.setLayout(layout)
        
        #on définit une scene
        self.scene = QtGui.QGraphicsScene()
        canvas.setScene(self.scene)

        #on définit le remplissage et les bords 
        self.pen = QtGui.QPen(QtGui.QColor("white"))
        self.brush = QtGui.QBrush(QtGui.QColor("black"))
        #on initialise la grille
        self.doBackground()

        
    def doBackground(self):
        """initialise la grille"""
        w = self.size().width()
        h = self.size().height()
        _item = self.scene.addRect(0, 0, w, h, self.pen,
                                  QtGui.QBrush(QtGui.QColor("white")))

    def addAnts(self, lAnts):
        """ lAnts = list of ants"""
        self.lAnts = lAnts
        self.lItems = []
        w = self.size().width()
        h = self.size().height()
        x = self.coordXmax
        y = self.coordYmax
        for ant in self.lAnts:
            [i, j] = ant.coord
            item = self.scene.addRect(i*w/x,j*h/y,w/x,h/y,QtGui.QPen(QtGui.QColor("white")),QtGui.QBrush(QtGui.QColor("black")))
            self.lItems.append(item)
        
    def animate(self):
        """ lAnts = list of ants"""
        w = self.size().width()
        h = self.size().height()
        x = self.coordXmax
        y = self.coordYmax
        for (nitem, ant) in enumerate(self.lAnts):
            [i, j] = ant.coord
            item = self.lItems[nitem]
            item.setRect(i*w/x, j*h/y, w/x, h/y)
