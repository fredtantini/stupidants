#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Colonie de fourmis
"""

from PyQt4 import QtGui
from PyQt4.QtCore import SIGNAL
from PyQt4.QtCore import QTimer

from display import GridDisplay
from ant import Ant

class Simu(QtGui.QMainWindow):
    """
    Classe principale pour les fourmis stupides.
    """

    def __init__(self):
        """ Initialisation """
        super(Simu, self).__init__()

        #coordonnées + grille
        self.xmax = 100
        self.ymax = 100
        self.wsize = 400
        self.hsize = 400

        #nb d'iterations
        self.compteur = 0
        self.maxCompteur = 4000

        #nb de fourmis
        self.nAnts = 20

        #taux de phéromones qu'une fourmi pose
        self.pheroUp = 50
        #taux d'évaporation des phéromones
        self.pheroDown = 0.1

        self.dictPhero = {} # { taux : [liste de [x, y]]}
        self.gridPhero = [[0 for _ in range(self.ymax)] for _ in range(self.xmax)]

        #point de départ et nourriture
        self.gridFood = [[0 for _ in range(self.ymax)] for _ in range(self.xmax)]
        self.gridFood[10][10] = 1
        startPos = [self.xmax/2, self.ymax/2]

        #no food on starting pos
        self.gridFood[startPos[0]][startPos[1]] = 0

        #le widget central est notre affichage de la grille
        self.resize(self.wsize, self.hsize)
        self.centralWidget = GridDisplay(self.xmax, self.ymax, self.wsize, self.hsize)
        self.setCentralWidget(self.centralWidget)

        self.centralWidget.populate(self.gridFood, startPos)
        
        #on initialise nos fourmis
        self.lAnts = []
        for _ in range(self.nAnts):        
            fourmi = Ant(self, startPos)
            self.lAnts.append(fourmi)

        #on ajoute la liste des foumis à notre grille
        self.centralWidget.addAnts(self.lAnts)


        #A chaque top du timer, on actualise la grille
        self.timer = QTimer()
        self.connect(self.timer, SIGNAL("timeout()"), self.animate)
        self.timer.start(100)
        
        
    def animate(self):
        """maj de la grille"""
        #atténuation des phéromones présentes
        newPhero = {}
        for lLev in self.dictPhero:
            newLev = lLev - self.pheroDown
            if (newLev > 0):
                newPhero[newLev] = self.dictPhero[lLev]
        self.dictPhero = newPhero

        #reconstruction de la grille des phéromones
        self.gridPhero = [[0 for _ in range(self.ymax)] for _ in range(self.xmax)]
        for lLev in self.dictPhero:
            for [i, j] in self.dictPhero[lLev]:
                self.gridPhero[i][j] = lLev
        
        self.compteur += 1
        for fourmi in self.lAnts:
            coord = fourmi.coord[:]
            if fourmi.goHome:
                ##ajout de la phéromone
                ###on récupère l'ancienne valeur
                oldPhero = self.gridPhero[coord[0]][coord[1]]
                ###s'il y avait des phéromones, on enlève l'ancienne valeur
                if oldPhero:
                    if fourmi.coord in self.dictPhero.get(oldPhero):
                        self.dictPhero.get(oldPhero).remove(fourmi.coord)
                ###on met à jour la nouvelle valeur
                self.gridPhero[coord[0]][coord[1]] = oldPhero + self.pheroUp
                self.dictPhero.setdefault(oldPhero + self.pheroUp, []).append(coord)

            ##déplacement de la fourmi
            fourmi.move()
            
        #maj de la grille
        self.centralWidget.animate(self.dictPhero)
        
        if self.compteur > self.maxCompteur:
            self.timer.stop()
            self.centralWidget.addAnts([Ant(None, [i, 0]) for i in range(self.xmax)])
            

import sys
def main(args):
    """ main """

    app = QtGui.QApplication(args)
    simu = Simu()
    simu.show()
    app.exec_()    


if __name__ == '__main__':
    main(sys.argv)
