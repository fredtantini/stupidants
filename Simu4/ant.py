#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Classe générique des fourmis
"""

def shorten(lwloop):
    """ On enlève les boucles de la liste """
    i = 0
    while i < len(lwloop):
        item = lwloop[i]
        lwloop.reverse()
        last = len(lwloop) - (lwloop.index(item) + 1)
        lwloop.reverse()
        lwloop.__delslice__(i, last)
        i = i + 1

from random import randint

class Ant:
    """
    Ce qui définit une fourmi et ce qu'elle sait faire
    """
    def __init__(self, parentGrid, startPos):
        """
        Une fourmi
        """
        self.coord = startPos
        self.pathDone = [self.coord]
        self.parentGrid = parentGrid
        #sur le chemin du retour ?
        self.goHome = False
        self.direction = randint(0,7)
        self.lastCoord = None

    def move(self):
        """
        Un pas, selon l'environnemnt.
        """
        #Si a trouvé de la nourriture
        if self.parentGrid.gridFood[self.coord[0]][self.coord[1]]:
            #retour a la maison
            self.goHome = True
            #sans faire de boucle
            shorten(self.pathDone)
            
        #si sur le chemin du retour
        if self.goHome:
            #et qu'il reste du chemin
            if self.pathDone:
                self.lastCoord = self.coord
                self.coord = self.pathDone.pop()
            else:
                #sinon, on repart
                self.goHome = False
        else:
            #si pas encore trouvé de nourritére
            directions = [(-1, -1), (-1, 0), (-1, 1),
                         (0, -1), (0, 1),
                         (1, -1), (1, 0), (1, 1)]
            gfood = self.parentGrid.gridFood
            food = [(gfood[(self.coord[0] + x) % self.parentGrid.xmax]
                         [(self.coord[1] + y) % self.parentGrid.ymax],
                         [(self.coord[0] + x) % self.parentGrid.xmax,
                          (self.coord[1] + y) % self.parentGrid.ymax])
                     for (x, y) in directions]
            food.sort()
            #si de la nourriture juste a coté, on y va
            if (food[-1][0]):
                self.lastCoord = self.coord
                self.coord =  food[-1][1]
            else:
                #sinon où sont les phéromones ?
                grid = self.parentGrid.gridPhero
                phero = [(grid[(self.coord[0] + x) % self.parentGrid.xmax]
                             [(self.coord[1] + y) % self.parentGrid.ymax],
                             [(self.coord[0] + x) % self.parentGrid.xmax,
                              (self.coord[1] + y) % self.parentGrid.ymax])
                         for (x,y) in directions]
                actualPhero = [(i, j) for (i, j) in phero if (i!= 0)]
                lenAP = len(actualPhero)
                if lenAP > 0:
                    #À 95%, on suit la plus forte phéromones
                        if (randint(0,100) < 95):
                            newCoord = actualPhero[randint(0,lenAP-1)][1]
                        else:
                            #sinon, on erre
                            newCoord = phero[randint(0,7)][1]
                else:
                    #s'il n'y a pas de phéromones à côté, on erre
                    newCoord = phero[randint(0,7)][1]
                while newCoord == self.lastCoord:
                    #mais on ne veut pas retourner sur nos pas!
                    newCoord = phero[randint(0,7)][1]
                self.lastCoord = self.coord
                self.coord = newCoord
                self.pathDone += [self.coord[:]]
        
        
