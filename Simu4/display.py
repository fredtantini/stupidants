#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Classe pour dessiner le terrain
"""

from PyQt4 import QtGui

class GridDisplay(QtGui.QWidget):
    """
    Affichage des fourmis, de la nourriture, etc.
    """

    def __init__(self, xmax, ymax, wsize, hsize):
        """ initialisation """
        super(GridDisplay, self).__init__()
        self.dimx = xmax
        self.dimy = ymax
        self.wsiz = wsize
        self.hsiz = hsize
        self.lAnts = []
        self.lItems = []
        self.lPhero = []
        
        #central widget pour la fenêtre principale
        cwidget = QtGui.QWidget(self)
        
        # On met un layout sur lequel ajouter le canvas (et des futurs boutons(?))
        layout = QtGui.QVBoxLayout()
        #le canvas qu'on ajoute au layout
        canvas = QtGui.QGraphicsView()
        layout.addWidget(canvas)
        #et on définit le layout du widget
        cwidget.setLayout(layout)
        
        #on définit une scene
        self.scene = QtGui.QGraphicsScene()
        canvas.setScene(self.scene)
        #avec ses dimensions
        canvas.setSceneRect(0, 0, self.wsiz, self.hsiz)

        #on définit le remplissage et les bords 
        self.pen = QtGui.QPen(QtGui.QColor("white"))
        self.brush = QtGui.QBrush(QtGui.QColor("black"))
        
    def populate(self, gridFood, startPos):
        """initialise la grille"""
        w = self.wsiz
        h = self.hsiz
        x = self.dimx
        y = self.dimy
        #on met le point de départ en rouge
        item = self.scene.addRect(startPos[0]*w/x, startPos[1]*h/y, w/x, h/y,
                                  self.pen, QtGui.QBrush(QtGui.QColor("red")))
        #que l'on passe en avant
        item.setZValue(2)
        #on place de la même façon la nourriture, en vert
        for i in range(len(gridFood)):
            for j in range(len(gridFood[0])):
                if gridFood[i][j]:
                    item = self.scene.addRect(i*w/x, j*h/y, w/x, h/y, self.pen,
                                  QtGui.QBrush(QtGui.QColor("green")))
                    item.setZValue(2)
                    

    def addAnts(self, lAnts):
        """ lAnts = list of ants"""
        self.lAnts = lAnts
        w = self.wsiz
        h = self.hsiz
        x = self.dimx
        y = self.dimy
        for ant in self.lAnts:
            [i, j] = ant.coord
            item = self.scene.addRect(i*w/x, j*h/y, w/x, h/y, self.pen, self.brush)
            self.lItems.append(item)
        
    def animate(self, dictPhero):
        """ lAnts = list of ants"""
        w = self.wsiz
        h = self.hsiz
        x = self.dimx
        y = self.dimy

        #on supprime de la scène les anciens carrés de phéromones
        if self.lPhero:
            for phero in self.lPhero:
                self.scene.removeItem(phero)
                self.lPhero.remove(phero)

        #et on ajoute les nouveaux
        for lLev in dictPhero:
            for phero in dictPhero[lLev]:
                item = self.scene.addRect(phero[0]*w/x, phero[1]*h/y, w/x, h/y, self.pen,
                                   QtGui.QBrush(QtGui.QColor(max(0, 255 - lLev), 255, 255)))
                item.setZValue(1)
                self.lPhero.append(item)

        #et on déplace les fourmis
        for (nitem, ant) in enumerate(self.lAnts):
            [i, j] = ant.coord
            item = self.lItems[nitem]
            item.setRect(i*w/x, j*h/y, w/x, h/y)
            #que l'on place au premier plan
            item.setZValue(3)
            
