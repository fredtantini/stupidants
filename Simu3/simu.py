#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Colonie de fourmis
"""

from PyQt4 import QtGui
from PyQt4.QtCore import Qt
from PyQt4.QtCore import SIGNAL
from PyQt4.QtCore import QTimer
from random import randint

from display import GridDisplay
from ant import Ant

class Simu(QtGui.QMainWindow):
    """
    Classe principale pour les fourmis stupides.
    """

    def __init__(self):
        """ Initialisation """
        super(Simu, self).__init__()

        #coordonnées + grille
        self.xmax = 50
        self.ymax = 50
        self.dim = (self.xmax, self.ymax)
        self.phero = {} # { taux : [liste de [x, y]]}

        #le widget central est notre affichage de la grille
        self.centralWidget = GridDisplay(self.dim)
        self.setCentralWidget(self.centralWidget)
        self.resize(480, 480)
        
        #on initialise nos fourmis
        self.nAnts = 30
        self.lAnts = []
        for _ in range(self.nAnts):        
            fourmi = Ant(self.centralWidget, randint(0, self.xmax - 1), randint(0, self.ymax - 1))
            self.lAnts.append(fourmi)
      
        #on ajoute la liste des foumis à notre grille
        self.centralWidget.addAnts(self.lAnts)
        
        #A chaque top du timer, on actualise la grille
        self.timer = QTimer()
        self.connect(self.timer, SIGNAL("timeout()"), self.animate)
        self.compteur = 0
        self.timer.start(100)
        self.animate()
        
        
    def animate(self):
        """maj de la grille"""
        #atténuation des phéromones présentes (5 de moins à chaque endroit)
        newPhero = {}
        for lLev in self.phero:
            newLev = lLev - 5
            if (newLev > 0):
                newPhero[newLev] = self.phero[lLev]
        self.phero = newPhero

        #reconstruction de la grille des phéromones
        gridPhero = [[0 for _ in range(self.ymax)] for _ in range(self.xmax)]
        for lLev in self.phero:
            for [i, j] in self.phero[lLev]:
                gridPhero[i][j] = lLev
        
        self.compteur += 1
        for fourmi in self.lAnts:
            ##ajout de la phéromone
            ###on récupère l'ancienne valeur
            oldPhero = gridPhero[fourmi.coord[0]][fourmi.coord[1]]
            ###s'il y avait des phéromones, on enlève l'ancienne valeur
            if oldPhero:
                if fourmi.coord in self.phero.get(oldPhero):
                    self.phero.get(oldPhero).remove(fourmi.coord)
            ###on met à jour avec la nouvelle valeur (50 de plus)
            gridPhero[fourmi.coord[0]][fourmi.coord[1]] = oldPhero + 50
            self.phero.setdefault(oldPhero + 50, []).append(fourmi.coord[:])

            ##déplacement de la fourmi
            fourmi.move()
            
        #maj de la grille
        self.centralWidget.animate(self.phero)
        
        if self.compteur > 400:
            self.timer.stop()
        for l in sorted(self.phero.keys()):
            print l, self.phero[l]
        print


import sys
def main(args):
    """ main """

    app = QtGui.QApplication(args)
    simu = Simu()
    simu.show()
    app.exec_()    


if __name__ == '__main__':
    main(sys.argv)
