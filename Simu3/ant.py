#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Classe générique des fourmis
"""

from random import randint

class Ant:
    """
    Ce qui définit une fourmi et ce qu'elle sait faire
    """
    def __init__(self, parentGrid, posx, posy):
        """
        Une fourmi
        """
        self.coord = [posx, posy]
        self.pathDone = [self.coord]
        self.parent = parentGrid

    def move(self):
        """
        Un pas, selon l'environnemnt.
        """
        directions = [(-1, -1), (-1, 0), (-1, 1),
                     (0, -1), (0, 1),
                     (1, -1), (1, 0), (1, 1)]
        direction = directions[randint(0, 7)]
        self.coord[0] += direction[0]
        self.coord[0] %= self.parent.coordXmax
        self.coord[1] += direction[1]
        self.coord[1] %= self.parent.coordYmax
        self.pathDone += [self.coord[:]]

        
class PourTest:
    """
    Une grille pour tester
    """
    def __init__(self, maxX, maxY):
        """ les coordonnées max de la grille"""
        self.coordXmax = maxX
        self.coordYmax = maxY


if __name__ == '__main__':
    """
    Pour tester
    """
    xmax = 10
    ymax = 5
    grille = [[0 for _ in range(ymax)][:] for _ in range(xmax)] #on initialise la grille
    parent = PourTest(xmax, ymax) #pour l'affichage
    nAnts = 10
    lAnts = [] #liste de nos fourmis
    for _ in range(nAnts): #pour i de 0 à 9
        fourmi = Ant(parent, randint(0, xmax - 1), randint(0, ymax - 1)) #on crée une fourmi
        grille[fourmi.coord[0]][fourmi.coord[1]] += 1 #on l'ajoute à la grille
        lAnts.append(fourmi) #et à la liste 
    niterations = 10
    from time import sleep
    for _ in range(niterations):
        #affichage (à -90°) de la grille 
        for ligne in grille:
            print ligne
        #pour chaque fourmi de la liste
        for fourmi in lAnts:
            ##suppression de la fourmi dans la grille
            grille[fourmi.coord[0]][fourmi.coord[1]] -= 1
            ##déplacement de la fourmi
            fourmi.move()
            ##ajout de la fourmi dans la grille
            grille[fourmi.coord[0]][fourmi.coord[1]] += 1
        sleep(1)
        print "=" * 40
 
        
