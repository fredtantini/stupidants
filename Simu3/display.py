#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Classe pour dessiner le terrain
"""

from PyQt4 import QtGui
from PyQt4.QtCore import Qt

class GridDisplay(QtGui.QWidget):
    """
    Affichage des fourmis, de la nourriture, etc.
    """

    def __init__(self, dim):
        """ initialisation """
        super(GridDisplay, self).__init__()
        self.dim = dim
        self.coordXmax = self.dim[0]
        self.coordYmax = self.dim[1]
        self.lAnts = []
        self.lItems = []
        self.lPhero = []
        
        #central widget pour la fenêtre principale
        cwidget = QtGui.QWidget(self)
        
        # On met un layout sur lequel ajouter le canvas (et des futurs boutons(?))
        layout = QtGui.QVBoxLayout()
        #le canvas qu'on ajoute au layout
        canvas = QtGui.QGraphicsView()
        layout.addWidget(canvas)
        #et on définit le layout du widget
        cwidget.setLayout(layout)
        
        #on définit une scene
        self.scene = QtGui.QGraphicsScene()
        canvas.setScene(self.scene)

        #on définit le remplissage et les bords 
        self.pen = QtGui.QPen(QtGui.QColor("white"))
        self.brush = QtGui.QBrush(QtGui.QColor("black"))
        #on initialise la grille
        self.doBackground()

        
    def doBackground(self):
        """initialise la grille"""
        w = self.size().width()
        h = self.size().height()
        _item = self.scene.addRect(0, 0, w, h, self.pen,
                                  QtGui.QBrush(QtGui.QColor("white")))

    def addAnts(self, lAnts):
        """ lAnts = list of ants"""
        self.lAnts = lAnts
        w = self.size().width()
        h = self.size().height()
        x = self.coordXmax
        y = self.coordYmax
        for ant in self.lAnts:
            [i, j] = ant.coord
            item = self.scene.addRect(i*w/x, j*h/y, w/x, h/y, self.pen, self.brush)
            self.lItems.append(item)
        
    def animate(self, dictPhero):
        """ lAnts = list of ants"""
        w = self.size().width()
        h = self.size().height()
        x = self.coordXmax
        y = self.coordYmax
        
        #on supprime de la scène les anciens carrés de phéromones
        if self.lPhero:
            print len(self.lPhero)
            for phero in self.lPhero:
                self.scene.removeItem(phero)
                self.lPhero.remove(phero)

        #et on ajoute les nouveaux
        for lLev in dictPhero:
            for phero in dictPhero[lLev]:
                item = self.scene.addRect(phero[0]*w/x, phero[1]*h/y, w/x, h/y, self.pen,
                                   QtGui.QBrush(QtGui.QColor(max(0, 255 - lLev), 255, 255)))
                item.setZValue(1)
                self.lPhero.append(item)

        #et on déplace les fourmis
        for (nitem, ant) in enumerate(self.lAnts):
            [i, j] = ant.coord
            item = self.lItems[nitem]
            item.setRect(i*w/x, j*h/y, w/x, h/y)
            #que l'on place au premier plan
            item.setZValue(2)
            
