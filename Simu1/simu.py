#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Colonie de fourmis
"""

from PyQt4 import QtGui
from PyQt4.QtCore import Qt
from PyQt4.QtCore import SIGNAL
from PyQt4.QtCore import QTimer
from random import randint

from display import GridDisplay
from ant import Ant

class Simu(QtGui.QMainWindow):
    """
    Classe principale pour les fourmis stupides.
    """

    def __init__(self):
        """ Initialisation """
        super(Simu, self).__init__()

        #coordonnées + grille
        self.xmax = 50
        self.ymax = 50
        self.grille = [[0 for _ in range(self.ymax)][:] for _ in range(self.xmax)]

        #le widget central est notre affichage de la grille
        self.centralWidget = GridDisplay(self.grille)
        self.setCentralWidget(self.centralWidget)
        self.resize(480, 480)
        
        #on initialise nos fourmis
        self.nAnts = 30
        self.lAnts = []
        for _ in range(self.nAnts):        
            fourmi = Ant(self.centralWidget, randint(0, self.xmax - 1), randint(0, self.ymax - 1))
            self.grille[fourmi.coord[0]][fourmi.coord[1]] += 1
            self.lAnts.append(fourmi)
        
        #A chaque top du timer, on actualise la grille
        self.timer = QTimer()
        self.connect(self.timer, SIGNAL("timeout()"), self.animate)
        self.compteur = 0
        #C'est parti pour toutes les 10ms
        self.timer.start(10)
        
    def animate(self):
        """maj de la grille"""
        self.compteur += 1
        for fourmi in self.lAnts:
            #déplacement de la fourmi
            ##suppression de la fourmi dans la grille
            self.grille[fourmi.coord[0]][fourmi.coord[1]] -= 1
            ##déplacement de la fourmi
            fourmi.move()
            self.grille[fourmi.coord[0]][fourmi.coord[1]] += 1
        self.centralWidget.grille = self.grille
        self.centralWidget.update()

        if self.compteur > 50:
            self.timer.stop()


import sys
def main(args):
    """ main """
    app = QtGui.QApplication(args)
    simu = Simu()
    simu.show()
    app.exec_()    

if __name__ == '__main__':
    main(sys.argv)
