#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Classe pour dessiner le terrain
"""

from PyQt4 import QtGui
from PyQt4.QtCore import Qt

class GridDisplay(QtGui.QWidget):
    """
    Affichage des fourmis, de la nourriture, etc.
    """

    def __init__(self, grilleParent):
        """ initialisation """
        super(GridDisplay, self).__init__()
        self.grille = grilleParent
        self.coordXmax = len(self.grille)
        self.coordYmax = len(self.grille[0])
        
    def paintEvent(self, event):
        """re-implementation de la fonction paintEvent()"""
        painter = QtGui.QPainter()
        painter.begin(self)

        w = self.size().width()
        h = self.size().height()
        x = self.coordXmax
        y = self.coordYmax
        
        for i in range(x):
            for j in range(y):
                #c = self.grille[i][j] # pour le test
                c = max(255 - self.grille[i][j] * 255, 0)
                painter.setBrush(QtGui.QBrush(QtGui.QColor(c, c, c))) #Comment c'est rempli
                painter.setPen(QtGui.QPen(QtGui.QColor("white"))) #Couleur des bords
                painter.drawRect(i*w/x, j*h/y, w/x, h/y)

        painter.end()

if __name__ == '__main__':
    """Pour tester"""
    from random import randint
    app = QtGui.QApplication([])
    xmax = 10
    ymax = 5
    grille = [[randint(0,255) for _ in range(ymax)][:] for _ in range(xmax)]
    grd = GridDisplay(grille)
    grd.show()
    app.exec_()    
